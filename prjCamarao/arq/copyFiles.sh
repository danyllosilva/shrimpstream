#!/bin/bash

## $ %SPARK_HOME%/bin/spark-submit --class WordCount --master local[*] --verbose Sample-0.0.1-SNAPSHOT.jar

if [ -z $1 ]
then
	echo 'usage:' $0 'intervalo_entre_copias_em_segundos'
	exit
else
	BASE=/var/camarao
	ORIGEM=$BASE/dataset
	DESTINO=$BASE/data_stream

	echo 'limpando diretório de destino...'
	rm $DESTINO/*
	echo 'Iniciando cópias...'
	for arq in `ls $ORIGEM/*.csv`
	do
		echo 'Arquivo:' $arq
		cp $arq $DESTINO/
		sleep $1
	done
	echo 'Fim!'
fi

