package bigdata.pesc

import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.SparkSession
import org.apache.log4j.{ Level, Logger }
import org.apache.spark.sql.types._
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.Encoder
import org.apache.spark.sql.streaming.OutputMode._
import org.apache.spark.sql.streaming.ProcessingTime
import scala.concurrent.duration._
import java.io.PrintStream
import java.net.ServerSocket
import org.apache.spark._
import bigdata.pesc.model.ReadSensorLocation
import scala.Array
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.streaming.OutputMode
import java.util.concurrent.TimeUnit
import org.apache.spark.sql.streaming.OutputMode
import bigdata.pesc.model.ReadSensor

object AVGReadSensorElasticsearch {

  def main(args: Array[String]) {
    if (args.length < 1) {
      System.err.println("Usage: Data stream directory")
      System.exit(1)
    }

    val diretorio: String = args(0)
    val elasticIndex: String = args(1) // camarao/novo
    val checkpointLocation: String = args(2) // /var/camarao/spark/checkpoint_camarao
 
       val spark = SparkSession
      .builder
      .master("local[*]")
      .appName("Camarao Streaming")
      .getOrCreate()

    import spark.implicits._

    val esquema = StructType(
      StructField("time_stamp", StringType, true) ::
        StructField("propertie", StringType, true) ::
        StructField("value", StringType, true) ::
        StructField("measure", StringType, true) ::
        StructField("measurement_type", StringType, true) ::
        StructField("water_type", StringType, true) ::
        StructField("location", StringType, true) ::
        StructField("latitude", StringType, true) ::
        StructField("longitude", StringType, true) ::
        StructField("idUser", StringType, true) ::
        StructField("responsible", StringType, true) ::
        StructField("email", StringType, true) ::
        StructField("gender", StringType, true) ::
        StructField("ipAddress", StringType, true) :: Nil)

    val leituras = spark.readStream.option("header", "true").schema(esquema).csv(diretorio)
      .select(
        unix_timestamp($"time_stamp").cast("timestamp") as "time_stamp", $"propertie", $"value", $"measure", $"measurement_type", $"water_type", $"location", $"responsible").as[ReadSensor]

    // media das leituras
    val contagens = leituras
      .withWatermark("time_stamp", "1 minutes")
      .groupBy(window($"time_stamp", "1 minutes"), $"propertie", $"measure", $"measurement_type", $"water_type", $"location", $"responsible")
      .agg(avg("value").as("avg"))
      .select($"window.start" as "start", $"window.end" as "end", date_format($"window.start", "y-MM-dd'T'hh:mm:ss.SSS'Z'") as "data", $"propertie", $"measure", $"measurement_type", $"water_type", $"location", $"responsible", $"avg")
      
     val query =  contagens.writeStream
        .format("es")
        .option("checkpointLocation", checkpointLocation)
        .start(elasticIndex)

      query.awaitTermination()

  }
}