package bigdata.pesc.test

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession

object AVGReading {

  def main(args: Array[String]) {
    
    val spark = SparkSession
      .builder
      .master("local[*]")
      .appName("File parquet")
      .getOrCreate()
      
    Logger.getLogger("org").setLevel(Level.ERROR)

    val ht = spark.read.csv("/home/danyllo/Documentos/apps/Others/tedb1/jobs-spark/prjCamarao/arq/result_avg")
    ht.take(3).foreach(println)
    
  }
}