package bigdata.pesc.model

case class ReadSensor(time_stamp:String, propertie:String, value:String, measure:String, measurement_type:String, water_type:String, location:String, responsible:String)