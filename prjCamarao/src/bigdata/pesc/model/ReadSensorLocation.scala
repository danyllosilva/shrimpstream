package bigdata.pesc.model

case class ReadSensorLocation(time_stamp:String, propertie:String, value:String, measure:String, measurement_type:String, water_type:String, location:String, responsible:String, latitude:String, longitude:String)