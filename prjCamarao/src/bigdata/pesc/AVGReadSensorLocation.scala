package bigdata.pesc

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.functions.unix_timestamp
import org.apache.spark.sql.functions.window
import org.apache.spark.sql.streaming.OutputMode.Append
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

import bigdata.pesc.model.ReadSensorLocation

object AVGReadSensorLocation {

  def main(args: Array[String]) {
    if (args.length < 1) {
      System.err.println("Usage: Data stream directory")
      System.exit(1)
    }

    val diretorio: String = args(0)

    val conf = new SparkConf()
      .setAppName(getClass.getName)
      .setIfMissing("spark.master", "local[*]")

    val spark = SparkSession
      .builder
      .config(conf)
      .getOrCreate()

    import spark.implicits._

    val esquema = StructType(
      StructField("time_stamp", StringType, true) ::
        StructField("propertie", StringType, true) ::
        StructField("value", StringType, true) ::
        StructField("measure", StringType, true) ::
        StructField("measurement_type", StringType, true) ::
        StructField("water_type", StringType, true) ::
        StructField("location", StringType, true) ::
        StructField("latitude", StringType, true) ::
        StructField("longitude", StringType, true) ::
        StructField("idUser", StringType, true) ::
        StructField("responsible", StringType, true) ::
        StructField("email", StringType, true) ::
        StructField("gender", StringType, true) ::
        StructField("ipAddress", StringType, true) :: Nil)

    val leituras = spark.readStream.option("header", "true").schema(esquema).csv(diretorio)
      .select(
        unix_timestamp($"time_stamp").cast("timestamp") as "time_stamp", $"propertie", $"value", $"measure", $"measurement_type", $"water_type", $"location", $"responsible").as[ReadSensorLocation]

    // media das leituras
    val contagens = leituras
      .withWatermark("time_stamp", "10 minutes")
      .groupBy(window($"time_stamp", "10 minutes"), $"propertie", $"measure", $"measurement_type", $"water_type", $"location", $"responsible")
      .agg(avg("value").as("avg"))
      .select($"window.start" as "start", $"window.end" as "end", $"propertie", $"measure", $"measurement_type", $"water_type", $"location", $"responsible", $"avg")

    // quantidade leituras
    /* val contagens = leituras
      .withWatermark("time_stamp", "10 minutes")
      .groupBy(window($"time_stamp", "10 minutes"), $"propertie", $"measure", $"measurement_type", $"water_type", $"location", $"responsible")
      .count()
      .select($"window.start" as "start", $"window.end" as "end", $"propertie", $"measure", $"measurement_type", $"water_type", $"location", $"responsible", $"count" as "readings")
      */
    val query = contagens.writeStream
      .outputMode(Append)
      .format("csv")
      .trigger(Trigger.ProcessingTime("5 seconds"))
      //  .trigger(Trigger.Continuous("1 second"))
      .option("path", "/home/danyllo/Documentos/apps/Others/tedb1/jobs-spark/prjCamarao/arq/result_avg/")
      .option("checkpointLocation", "/home/danyllo/Documentos/apps/Others/tedb1/jobs-spark/prjCamarao/arq/checkpoint")
      .start()
    /*
    val query = contagens.writeStream
      .outputMode("complete")
      .format("console")
      .start
*/
    query.awaitTermination()

  }
}